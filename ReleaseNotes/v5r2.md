2025-02-13 Allen v5r2
===

This version uses
Rec [v37r2](../../../../Rec/-/tags/v37r2),
Lbcom [v36r3](../../../../Lbcom/-/tags/v36r3),
LHCb [v56r3](../../../../LHCb/-/tags/v56r3),
Gaudi [v39r2](../../../../Gaudi/-/tags/v39r2),
Detector [v2r3](../../../../Detector/-/tags/v2r3) and
LCG [105c](http://lcginfo.cern.ch/release/105c/) .


This version is released on the `master` branch.
Built relative to Allen [v5r1](/../../tags/v5r1), with the following changes:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Decoding | Tests to compare CPU and GPU decoding for Plume, !1111 (@espedica) [#379]


### Other

- ~Monitoring | Saving counters result as monitoring histogram, !1856 (@vkholoim)
- Made monitoring file names unique in mdf_input tests, !1884 (@sponce)
- Remove unused MonitorManager and associated monitor thread, !1883 (@ahennequ)
- Fix counters histogram being registered multiple time, !1881 (@ahennequ) [MooreOnline#94]
- Update References for: Allen!1856 based on lhcb-master-mr/12363, !1878 (@lhcbsoft)
- Increase timeouts for Allen TCK tests, !1877 (@jonrob)
- Speedup of FilterSVTracks by reordering loop., !1876 (@leuecker)
- Synchronize master branch with 2024-patches, !1826 (@clemenci)
- Include BB filter in ODQV, !1578 (@anmorris)
- Fix refs for allen_event_loop, !1873 (@jonrob)
- Fix incorrect indent level in BinaryDumpers/options/allen.py, !1869 (@jonrob)
