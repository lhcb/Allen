2024-04-19 Allen v4r7
===

This version uses
Rec [v36r7](../../../../Rec/-/tags/v36r7),
Lbcom [v35r7](../../../../Lbcom/-/tags/v35r7),
LHCb [v55r7](../../../../LHCb/-/tags/v55r7),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r31](../../../../Detector/-/tags/v1r31) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Allen [v4r6](/../../tags/v4r6), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- Fix error banks line, !1565 (@raaij)
- Fix D* histogram names, !1560 (@thboettc) [#530]


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- ~Monitoring | Monitoring bug fixes, !1561 (@kaaricha)
- Update Allen!1566 references manually, !1576 (@cagapopo)
- Fix dimuon mass hypo on DiMuonNoIPLine monitoring, !1575 (@acasaisv)
- Update refs Allen!1559, !1574 (@cagapopo)
- Reduce PT and tighten corrChi2 of DiMuonHighMass, !1570 (@cmarinbe)
- Add Monet plots for D0 pp and SMOG trigger lines, !1567 (@samarian)
- Propagate V4r6 hotfixes on 2024-patches, !1566 (@cagapopo)
- Veto shared hitson DiMuonNoIP, !1559 (@acasaisv)
- Fix `TwoTrackKs_minEta_Ks` in thresholds, !1549 (@lpica)
- Minor monitoring updates (new format), !1503 (@kaaricha)
- Update References for: Allen!1523 based on lhcb-2024-patches-mr/5, !1555 (@lhcbsoft)
- Add prompt dihadron container, !1523 (@thboettc)
