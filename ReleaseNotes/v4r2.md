2024-03-05 Allen v4r2
===

This version uses
Rec [v36r2](../../../../Rec/-/tags/v36r2),
Lbcom [v35r2](../../../../Lbcom/-/tags/v35r2),
LHCb [v55r2](../../../../LHCb/-/tags/v55r2),
Gaudi [v38r0](../../../../Gaudi/-/tags/v38r0),
Detector [v1r27](../../../../Detector/-/tags/v1r27) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) .

This version is released on the `master` branch.
Built relative to Allen [v4r1](/../../tags/v4r1), with the following changes:

### New features ~"new feature"



### Fixes ~"bug fix" ~workaround

- ~Persistency | Better HLT1 persistency, !1462 (@cagapopo)
- Fix cluster count offsets, !1470 (@cagapopo)
- Fix crash when Hlt1LambdaLLDetachedTrack or Hlt1XiOmegaLLL are run alone, !1451 (@kaaricha)
- Fix pipeline, !1447 (@anmorris)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing



### Documentation ~Documentation


### Other

- ~Decoding | New decoding for plume, !1326 (@espedica)
- Update References for: Allen!1377 based on lhcb-master-mr/10779, !1465 (@lhcbsoft)
- TAE activity filter, !1443 (@cagapopo)
- Make BeamlineOffset configurable for Allen-in-Moore, !1441 (@dovombru) [#492]
- Add beam-beam filter to all lines that didn't have a filter on BX type yet., !1428 (@raaij) [#338]
- Update the Calo Clustering algorithm in HLT1, !1410 (@ahennequ)
- Fix hardcoded geometry in forward/seeding algorithms, !1377 (@ascarabo)
- Add z-specific tomography lines to enhance downstream z, !1192 (@samarian)
- Fix Velo clustering Warnings, !1460 (@ahennequ)
- Add VeloMicroBiasLine for VELO closure (prefiltered on VeloOpen event type), !1445 (@samarian)
- Sequences for VeloSP, !1427 (@oozcelik) [#493]
- Remove usage of isolation flag in SP velo decoding, !1426 (@ahennequ)
- Configurable velo-UT Ghost Track Cut, !1415 (@dtou)
- Fix Allen UT geometry, !1407 (@jzhuo) [#467]
- Update DumpVPGeometry.cpp, !1406 (@ascarabo)
- Added DiElectron High Mass lines, !1315 (@ldufour)
- Manual update for Allen ci references, !1440 (@cagapopo)
- Update routing bit 25 for events with error banks, !1429 (@raaij)
