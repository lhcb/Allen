2024-08-29 Allen v4r12p1
===

This version uses
Rec [v36r12p1](../../../../Rec/-/tags/v36r12p1),
Lbcom [v35r12](../../../../Lbcom/-/tags/v35r12),
LHCb [v55r12](../../../../LHCb/-/tags/v55r12),
Detector [v1r35](../../../../Detector/-/tags/v1r35),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Allen [v4r12](/../../tags/v4r12), with the following changes:

- Fix device detection to align with nvidia-smi, !1752 (@raaij)
