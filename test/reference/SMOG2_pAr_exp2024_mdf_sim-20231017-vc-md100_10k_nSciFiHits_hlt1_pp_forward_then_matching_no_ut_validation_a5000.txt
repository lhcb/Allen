forward_validator validation:
TrackChecker output                               :         3/      414   0.72% ghosts
for P>3GeV,Pt>0.5GeV                              :         3/      414   0.72% ghosts
01_long                                           :       396/     4578   8.65% (  7.46%),         9 (  2.22%) clones, pur  99.95%, hit eff  98.59%
02_long_P>5GeV                                    :       396/     3038  13.03% ( 11.17%),         9 (  2.22%) clones, pur  99.95%, hit eff  98.59%
03_long_strange                                   :         8/      226   3.54% (  3.37%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.08%
04_long_strange_P>5GeV                            :         8/      116   6.90% (  6.63%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.08%
07_long_electrons                                 :         1/      595   0.17% (  0.38%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
08_long_electrons_P>5GeV                          :         1/      341   0.29% (  0.50%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
long_P>5GeV_AND_Pt>1GeV                           :       253/      281  90.04% ( 88.83%),         7 (  2.69%) clones, pur  99.99%, hit eff  99.66%
11_noVelo_UT                                      :         0/      643   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/      270   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :        13/       14  92.86% ( 96.97%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
15_long_strange_P>5GeV                            :         8/      116   6.90% (  6.63%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.08%
16_long_strange_P>5GeV_PT>500MeV                  :         8/       35  22.86% ( 22.58%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.08%
18_long_nSciFiHits_gt_0_AND_lt_5000               :       397/     5173   7.67% (  6.53%),         9 (  2.22%) clones, pur  99.95%, hit eff  98.60%


long_validator validation:
TrackChecker output                               :        89/     3785   2.35% ghosts
for P>3GeV,Pt>0.5GeV                              :         8/     1320   0.61% ghosts
01_long                                           :      3236/     4578  70.69% ( 69.61%),        80 (  2.41%) clones, pur  99.92%, hit eff  98.71%
02_long_P>5GeV                                    :      2663/     3038  87.66% ( 87.18%),        65 (  2.38%) clones, pur  99.94%, hit eff  99.22%
03_long_strange                                   :       151/      226  66.81% ( 65.94%),         3 (  1.95%) clones, pur  99.81%, hit eff  98.48%
04_long_strange_P>5GeV                            :       103/      116  88.79% ( 88.55%),         1 (  0.96%) clones, pur  99.85%, hit eff  98.89%
07_long_electrons                                 :       189/      595  31.76% ( 33.14%),        12 (  5.97%) clones, pur  98.67%, hit eff  98.72%
08_long_electrons_P>5GeV                          :       157/      341  46.04% ( 46.86%),        12 (  7.10%) clones, pur  98.72%, hit eff  98.96%
long_P>5GeV_AND_Pt>1GeV                           :       265/      281  94.31% ( 94.37%),         7 (  2.57%) clones, pur  99.99%, hit eff  99.55%
11_noVelo_UT                                      :         0/      643   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/      270   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :        13/       14  92.86% ( 96.97%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
15_long_strange_P>5GeV                            :       103/      116  88.79% ( 88.55%),         1 (  0.96%) clones, pur  99.85%, hit eff  98.89%
16_long_strange_P>5GeV_PT>500MeV                  :        34/       35  97.14% ( 96.77%),         1 (  2.86%) clones, pur 100.00%, hit eff  98.61%
18_long_nSciFiHits_gt_0_AND_lt_5000               :      3425/     5173  66.21% ( 64.99%),        92 (  2.62%) clones, pur  99.85%, hit eff  98.71%


muon_validator validation:
Muon fraction in all MCPs:                                                831/    74514   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                     28/     4177   0.01% 
Correctly identified muons with isMuon:                                    28/       28 100.00% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                       0/        0   -nan% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:       106/     4149   2.55% 
Ghost tracks identified as muon with isMuon:                                1/       89   1.12% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.924 (   452/   489)
Isolated             :  0.944 (   437/   463)
Close                :  0.577 (    15/    26)
False rate           :  0.007 (     3/   455)
Real false rate      :  0.007 (     3/   455)
Clones               :  0.000 (     0/   452)


rate_validator validation:
Hlt1BGIPseudoPVsBeamOne:                            4/   500, (  240.00 +/-   119.52) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsIRBeamBeam:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet100GeV:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet15GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet30GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet50GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KK:                                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KPi:                                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KPiAlignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2Kshh:                                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2PiPi:                                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronDisplaced:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass:                             0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronHighMass_SS:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronSoft:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDisplaced:                                0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonJpsiMassAlignment:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP_SS:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonSoft:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiPhotonHighMass:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0Pi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0PiAlignment:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1GECPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1LambdaLLDetachedTrack:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINeeFarFromActivity:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1Passthrough:                                    0/   500, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1Pi02GammaGamma:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGeneric:                              3/   500, (  180.00 +/-   103.61) kHz
Hlt1SMOG22BodyGenericPrompt:                        3/   500, (  180.00 +/-   103.61) kHz
Hlt1SMOG2BELowMultElectrons:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BENoBias:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     3/   500, (  180.00 +/-   103.61) kHz
Hlt1SMOG2DiMuonHighMass:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DisplacedDiMuon:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                                  3/   500, (  180.00 +/-   103.61) kHz
Hlt1SMOG2L0Toppi:                                   3/   500, (  180.00 +/-   103.61) kHz
Hlt1SMOG2MinimumBias:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                       5/   500, (  300.00 +/-   133.49) kHz
Hlt1SMOG2SingleMuon:                                0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackHighPt:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2etacTopp:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtElectron:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuon:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuonNoMuID:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackMVA:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackMuonMVA:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1TwoKs:                                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackKs:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackMVA:                                    0/   500, (    0.00 +/-     0.00) kHz
Hlt1UpsilonAlignment:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  1/   500, (   60.00 +/-    59.94) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                                     0/   500, (    0.00 +/-     0.00) kHz
Inclusive:                                         24/   500, ( 1440.00 +/-   286.80) kHz


seed_validator validation:
TrackChecker output                               :        22/     6581   0.33% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :       921/     1413  65.18% ( 67.33%),         6 (  0.65%) clones, pur  99.73%, hit eff  98.42%
01_long                                           :      3297/     4578  72.02% ( 72.97%),        14 (  0.42%) clones, pur  99.81%, hit eff  98.56%
---1. phi quadrant                                :       813/     1114  72.98% ( 73.39%),         5 (  0.61%) clones, pur  99.79%, hit eff  98.62%
---2. phi quadrant                                :       848/     1183  71.68% ( 71.64%),         2 (  0.24%) clones, pur  99.84%, hit eff  98.40%
---3. phi quadrant                                :       793/     1112  71.31% ( 72.52%),         3 (  0.38%) clones, pur  99.78%, hit eff  98.60%
---4. phi quadrant                                :       843/     1169  72.11% ( 72.23%),         4 (  0.47%) clones, pur  99.82%, hit eff  98.60%
---eta < 2.5, small x, large y                    :         1/       59   1.69% (  2.08%),         0 (  0.00%) clones, pur 100.00%, hit eff  83.33%
---eta < 2.5, large x, small y                    :        18/       78  23.08% ( 25.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  94.82%
---eta > 2.5, small x, large y                    :      1218/     1629  74.77% ( 75.49%),         6 (  0.49%) clones, pur  99.83%, hit eff  98.20%
---eta > 2.5, large x, small y                    :      2060/     2812  73.26% ( 73.76%),         8 (  0.39%) clones, pur  99.80%, hit eff  98.81%
02_long_P>5GeV                                    :      2549/     3038  83.90% ( 85.81%),        10 (  0.39%) clones, pur  99.83%, hit eff  99.18%
02_long_P>5GeV, eta > 4                           :      1442/     1635  88.20% ( 87.96%),         7 (  0.48%) clones, pur  99.83%, hit eff  99.35%
---eta < 2.5, small x, large y                    :         1/        7  14.29% ( 14.29%),         0 (  0.00%) clones, pur 100.00%, hit eff  83.33%
---eta < 2.5, large x, small y                    :         2/        8  25.00% ( 25.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
---eta > 2.5, small x, large y                    :       964/     1109  86.93% ( 88.01%),         5 (  0.52%) clones, pur  99.82%, hit eff  99.06%
---eta > 2.5, large x, small y                    :      1582/     1914  82.65% ( 84.00%),         5 (  0.32%) clones, pur  99.83%, hit eff  99.26%
03_long_P>3GeV                                    :      3297/     3988  82.67% ( 83.58%),        14 (  0.42%) clones, pur  99.81%, hit eff  98.56%
04_long_P>0.5GeV                                  :      3297/     4578  72.02% ( 72.97%),        14 (  0.42%) clones, pur  99.81%, hit eff  98.56%
08_UT+SciFi                                       :       474/      935  50.70% ( 46.28%),         1 (  0.21%) clones, pur  99.71%, hit eff  97.09%
09_UT+SciFi_P>5GeV                                :       300/      334  89.82% ( 89.42%),         0 (  0.00%) clones, pur  99.79%, hit eff  97.90%
10_UT+SciFi_P>3GeV                                :       471/      618  76.21% ( 74.57%),         1 (  0.21%) clones, pur  99.71%, hit eff  97.07%
11_UT+SciFi_fromStrange                           :       103/      141  73.05% ( 72.57%),         0 (  0.00%) clones, pur  99.65%, hit eff  98.27%
12_UT+SciFi_fromStrange_P>5GeV                    :        83/       86  96.51% ( 97.85%),         0 (  0.00%) clones, pur  99.67%, hit eff  98.58%
13_UT+SciFi_fromStrange_P>3GeV                    :       103/      120  85.83% ( 84.31%),         0 (  0.00%) clones, pur  99.65%, hit eff  98.27%
14_long_electrons                                 :       314/      595  52.77% ( 53.52%),         3 (  0.95%) clones, pur  99.67%, hit eff  98.69%
15_long_electrons_P>5GeV                          :       251/      341  73.61% ( 73.80%),         3 (  1.18%) clones, pur  99.65%, hit eff  98.84%
16_long_electrons_P>3GeV                          :       314/      501  62.67% ( 62.77%),         3 (  0.95%) clones, pur  99.67%, hit eff  98.69%
19_long_PT>2GeV                                   :         1/       14   7.14% (  3.03%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
21_long_strange_P>5GeV                            :       106/      116  91.38% ( 90.96%),         1 (  0.93%) clones, pur  99.73%, hit eff  98.98%
22_long_strange_P>5GeV_PT>500MeV                  :        27/       35  77.14% ( 77.42%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.77%
24_noVelo+UT+T_fromKs0                            :        71/       92  77.17% ( 74.46%),         0 (  0.00%) clones, pur  99.74%, hit eff  98.57%
25_noVelo+UT+T_fromLambda                         :        32/       44  72.73% ( 68.97%),         0 (  0.00%) clones, pur  99.43%, hit eff  98.13%
27_noVelo+UT+T_fromKs0_P>5GeV                     :        52/       53  98.11% ( 98.86%),         0 (  0.00%) clones, pur  99.65%, hit eff  98.51%
28_noVelo+UT+T_fromLambda_P>5GeV                  :        27/       28  96.43% ( 95.24%),         0 (  0.00%) clones, pur  99.66%, hit eff  98.77%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :        23/       24  95.83% ( 97.83%),         0 (  0.00%) clones, pur  99.20%, hit eff  97.76%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :        14/       15  93.33% ( 91.67%),         0 (  0.00%) clones, pur  99.35%, hit eff  98.81%
35_long_nSciFiHits_gt_0_AND_lt_5000               :      4265/     6001  71.07% ( 71.33%),        18 (  0.42%) clones, pur  99.80%, hit eff  98.63%


seed_xz_validator validation:
TrackChecker output                               :       280/     8315   3.37% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :      1376/     1413  97.38% ( 97.64%),         9 (  0.65%) clones, pur  99.74%, hit eff  49.69%
01_long                                           :      3846/     4578  84.01% ( 84.03%),        19 (  0.49%) clones, pur  99.76%, hit eff  49.63%
---1. phi quadrant                                :       942/     1114  84.56% ( 85.36%),         6 (  0.63%) clones, pur  99.69%, hit eff  49.56%
---2. phi quadrant                                :       996/     1183  84.19% ( 84.28%),         4 (  0.40%) clones, pur  99.85%, hit eff  49.63%
---3. phi quadrant                                :       929/     1112  83.54% ( 84.20%),         2 (  0.21%) clones, pur  99.81%, hit eff  49.72%
---4. phi quadrant                                :       979/     1169  83.75% ( 84.02%),         7 (  0.71%) clones, pur  99.71%, hit eff  49.63%
---eta < 2.5, small x, large y                    :        13/       59  22.03% ( 25.69%),         0 (  0.00%) clones, pur  98.46%, hit eff  49.30%
---eta < 2.5, large x, small y                    :        28/       78  35.90% ( 36.72%),         0 (  0.00%) clones, pur 100.00%, hit eff  49.70%
---eta > 2.5, small x, large y                    :      1404/     1629  86.19% ( 86.43%),         6 (  0.43%) clones, pur  99.83%, hit eff  49.57%
---eta > 2.5, large x, small y                    :      2401/     2812  85.38% ( 85.51%),        13 (  0.54%) clones, pur  99.73%, hit eff  49.67%
02_long_P>5GeV                                    :      3001/     3038  98.78% ( 98.77%),        14 (  0.46%) clones, pur  99.79%, hit eff  49.73%
02_long_P>5GeV, eta > 4                           :      1622/     1635  99.20% ( 99.11%),         9 (  0.55%) clones, pur  99.75%, hit eff  49.79%
---eta < 2.5, small x, large y                    :         7/        7 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.65%
---eta < 2.5, large x, small y                    :         8/        8 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.00%
---eta > 2.5, small x, large y                    :      1097/     1109  98.92% ( 98.83%),         5 (  0.45%) clones, pur  99.81%, hit eff  49.62%
---eta > 2.5, large x, small y                    :      1889/     1914  98.69% ( 98.63%),         9 (  0.47%) clones, pur  99.78%, hit eff  49.78%
03_long_P>3GeV                                    :      3846/     3988  96.44% ( 96.20%),        19 (  0.49%) clones, pur  99.76%, hit eff  49.63%
04_long_P>0.5GeV                                  :      3846/     4578  84.01% ( 84.03%),        19 (  0.49%) clones, pur  99.76%, hit eff  49.63%
08_UT+SciFi                                       :       561/      935  60.00% ( 55.65%),         4 (  0.71%) clones, pur  99.73%, hit eff  49.58%
09_UT+SciFi_P>5GeV                                :       326/      334  97.60% ( 97.73%),         3 (  0.91%) clones, pur  99.71%, hit eff  49.75%
10_UT+SciFi_P>3GeV                                :       557/      618  90.13% ( 89.36%),         4 (  0.71%) clones, pur  99.73%, hit eff  49.59%
11_UT+SciFi_fromStrange                           :       112/      141  79.43% ( 79.53%),         0 (  0.00%) clones, pur  99.82%, hit eff  49.62%
12_UT+SciFi_fromStrange_P>5GeV                    :        84/       86  97.67% ( 98.66%),         0 (  0.00%) clones, pur  99.76%, hit eff  49.49%
13_UT+SciFi_fromStrange_P>3GeV                    :       112/      120  93.33% ( 92.97%),         0 (  0.00%) clones, pur  99.82%, hit eff  49.62%
14_long_electrons                                 :       326/      595  54.79% ( 55.44%),         3 (  0.91%) clones, pur  99.71%, hit eff  49.75%
15_long_electrons_P>5GeV                          :       260/      341  76.25% ( 76.30%),         2 (  0.76%) clones, pur  99.78%, hit eff  49.90%
16_long_electrons_P>3GeV                          :       326/      501  65.07% ( 65.01%),         3 (  0.91%) clones, pur  99.71%, hit eff  49.75%
19_long_PT>2GeV                                   :        14/       14 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  49.35%
21_long_strange_P>5GeV                            :       116/      116 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  49.77%
22_long_strange_P>5GeV_PT>500MeV                  :        35/       35 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.18%
24_noVelo+UT+T_fromKs0                            :        72/       92  78.26% ( 75.27%),         0 (  0.00%) clones, pur  99.72%, hit eff  49.40%
25_noVelo+UT+T_fromLambda                         :        36/       44  81.82% ( 82.76%),         0 (  0.00%) clones, pur 100.00%, hit eff  49.77%
27_noVelo+UT+T_fromKs0_P>5GeV                     :        53/       53 100.00% (100.00%),         0 (  0.00%) clones, pur  99.62%, hit eff  49.41%
28_noVelo+UT+T_fromLambda_P>5GeV                  :        27/       28  96.43% ( 95.24%),         0 (  0.00%) clones, pur 100.00%, hit eff  49.53%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :        24/       24 100.00% (100.00%),         0 (  0.00%) clones, pur  99.17%, hit eff  49.27%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :        14/       15  93.33% ( 91.67%),         0 (  0.00%) clones, pur 100.00%, hit eff  50.06%
35_long_nSciFiHits_gt_0_AND_lt_5000               :      4870/     6001  81.15% ( 80.89%),        27 (  0.55%) clones, pur  99.73%, hit eff  49.66%


selreport_validator validation:
                                               Events  Candidates
Hlt1BGIPseudoPVsBeamOne:                            4           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1BGIPseudoPVsIRBeamBeam:                         0           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BeamGas:                                        0           0
Hlt1ConeJet100GeV:                                  0           0
Hlt1ConeJet15GeV:                                   0           0
Hlt1ConeJet30GeV:                                   0           0
Hlt1ConeJet50GeV:                                   0           0
Hlt1D2KK:                                           0           0
Hlt1D2KPi:                                          0           0
Hlt1D2KPiAlignment:                                 0           0
Hlt1D2Kshh:                                         0           0
Hlt1D2PiPi:                                         0           0
Hlt1DetJpsiToMuMuNegTagLine:                        0           0
Hlt1DetJpsiToMuMuPosTagLine:                        0           0
Hlt1DiElectronDisplaced:                            0           0
Hlt1DiElectronHighMass:                             0           0
Hlt1DiElectronHighMass_SS:                          0           0
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         0           0
Hlt1DiElectronLowMass_massSlice1_prompt:            0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronSoft:                                 0           0
Hlt1DiMuonDisplaced:                                0           0
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonHighMass:                                 0           0
Hlt1DiMuonJpsiMassAlignment:                        0           0
Hlt1DiMuonNoIP:                                     0           0
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonSoft:                                     0           0
Hlt1DiPhotonHighMass:                               0           0
Hlt1Dst2D0Pi:                                       0           0
Hlt1Dst2D0PiAlignment:                              0           0
Hlt1ErrorBank:                                      0           0
Hlt1GECPassthrough:                                 0           0
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1LambdaLLDetachedTrack:                          0           0
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODINeeFarFromActivity:                          0           0
Hlt1OneMuonTrackLine:                               0           0
Hlt1Passthrough:                                    0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1Pi02GammaGamma:                                 0           0
Hlt1RICH1Alignment:                                 0           0
Hlt1RICH2Alignment:                                 0           0
Hlt1SMOG22BodyGeneric:                              3          12
Hlt1SMOG22BodyGenericPrompt:                        3           8
Hlt1SMOG2BELowMultElectrons:                        0           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2D2Kpi:                                     3           3
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2DisplacedDiMuon:                           0           0
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0           0
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0           0
Hlt1SMOG2KsTopipi:                                  3           3
Hlt1SMOG2L0Toppi:                                   3           3
Hlt1SMOG2MinimumBias:                               0           0
Hlt1SMOG2PassThroughLowMult5:                       5           0
Hlt1SMOG2SingleMuon:                                0           0
Hlt1SMOG2SingleTrackHighPt:                         0           0
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2etacTopp:                                  0           0
Hlt1SingleHighPtElectron:                           0           0
Hlt1SingleHighPtMuon:                               0           0
Hlt1SingleHighPtMuonNoMuID:                         0           0
Hlt1TAEPassthrough:                                 0           0
Hlt1TrackElectronMVA:                               0           0
Hlt1TrackMVA:                                       0           0
Hlt1TrackMuonMVA:                                   0           0
Hlt1TwoKs:                                          0           0
Hlt1TwoTrackKs:                                     0           0
Hlt1TwoTrackMVA:                                    0           0
Hlt1UpsilonAlignment:                               0           0
Hlt1VeloMicroBias:                                  1           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1XiOmegaLLL:                                     0           0

Total decisions:      25
Total tracks:         40
Total calos clusters: 0
Total SVs:            29
Total hits:           1057
Total stdinfo:        461


velo_validator validation:
TrackChecker output                               :       127/    13870   0.92% ghosts
01_velo                                           :      7506/     7639  98.26% ( 98.41%),       365 (  4.64%) clones, pur  99.57%, hit eff  94.25%
02_long                                           :      4545/     4578  99.28% ( 99.41%),       195 (  4.11%) clones, pur  99.79%, hit eff  94.98%
03_long_P>5GeV                                    :      3026/     3038  99.61% ( 99.58%),       111 (  3.54%) clones, pur  99.88%, hit eff  95.67%
04_long_strange                                   :       222/      226  98.23% ( 98.04%),        10 (  4.31%) clones, pur  99.54%, hit eff  95.03%
05_long_strange_P>5GeV                            :       113/      116  97.41% ( 97.19%),         1 (  0.88%) clones, pur  99.58%, hit eff  98.38%
08_long_electrons                                 :       585/      595  98.32% ( 98.39%),        55 (  8.59%) clones, pur  97.11%, hit eff  93.30%

