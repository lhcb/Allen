long_validator validation:
TrackChecker output                               :       705/    22620   3.12% ghosts
for P>3GeV,Pt>0.5GeV                              :       400/    15367   2.60% ghosts
01_long                                           :     20618/    37926  54.36% ( 55.75%),       355 (  1.69%) clones, pur  98.83%, hit eff  98.29%
02_long_P>5GeV                                    :     17344/    24714  70.18% ( 71.54%),       307 (  1.74%) clones, pur  98.95%, hit eff  98.60%
03_long_strange                                   :       625/     1807  34.59% ( 34.92%),         8 (  1.26%) clones, pur  98.58%, hit eff  97.81%
04_long_strange_P>5GeV                            :       477/      878  54.33% ( 54.32%),         6 (  1.24%) clones, pur  98.74%, hit eff  98.13%
05_long_fromB                                     :      1780/     2309  77.09% ( 77.50%),        29 (  1.60%) clones, pur  98.96%, hit eff  98.64%
06_long_fromB_P>5GeV                              :      1658/     1909  86.85% ( 86.77%),        27 (  1.60%) clones, pur  99.03%, hit eff  98.82%
07_long_electrons                                 :       399/     2697  14.79% ( 14.88%),        12 (  2.92%) clones, pur  97.64%, hit eff  97.64%
08_long_electrons_P>5GeV                          :       340/     1402  24.25% ( 25.25%),        11 (  3.13%) clones, pur  97.58%, hit eff  97.85%
09_long_fromB_electrons                           :        39/      110  35.45% ( 40.12%),         0 (  0.00%) clones, pur  98.17%, hit eff  97.66%
10_long_fromB_electrons_P>5GeV                    :        36/       74  48.65% ( 51.56%),         0 (  0.00%) clones, pur  98.02%, hit eff  97.70%
long_P>5GeV_AND_Pt>1GeV                           :      5758/     6486  88.78% ( 90.43%),        95 (  1.62%) clones, pur  98.88%, hit eff  98.76%
long_fromB_P>5GeV_AND_Pt>1GeV                     :      1163/     1281  90.79% ( 90.58%),        16 (  1.36%) clones, pur  98.99%, hit eff  98.87%
11_noVelo_UT                                      :         0/     4124   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     1671   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :      1464/     1586  92.31% ( 92.69%),        16 (  1.08%) clones, pur  98.83%, hit eff  98.98%
14_long_from_B_PT>2GeV                            :       552/      605  91.24% ( 91.39%),         2 (  0.36%) clones, pur  98.98%, hit eff  99.12%
15_long_strange_P>5GeV                            :       477/      878  54.33% ( 54.32%),         6 (  1.24%) clones, pur  98.74%, hit eff  98.13%
16_long_strange_P>5GeV_PT>500MeV                  :       290/      335  86.57% ( 86.17%),         3 (  1.02%) clones, pur  98.72%, hit eff  98.08%
17_long_fromSignal                                :      1130/     1312  86.13% ( 84.73%),        17 (  1.48%) clones, pur  99.01%, hit eff  98.76%
18_long_nSciFiHits_gt_0_AND_lt_5000               :      9222/    16638  55.43% ( 55.65%),       179 (  1.90%) clones, pur  98.96%, hit eff  98.76%
19_long_nSciFiHits_gt_5000_AND_lt_7000            :      7685/    14945  51.42% ( 51.68%),       118 (  1.51%) clones, pur  98.79%, hit eff  98.12%
20_long_nSciFiHits_gt_7000_AND_lt_10000           :      3900/     8507  45.84% ( 46.23%),        65 (  1.64%) clones, pur  98.47%, hit eff  97.49%
21_long_nSciFiHits_gt_10000                       :       136/      409  33.25% ( 33.24%),         3 (  2.16%) clones, pur  98.00%, hit eff  95.50%


muon_validator validation:
Muon fraction in all MCPs:                                               7257/   574938   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    237/    25535   0.01% 
Correctly identified muons with isMuon:                                   185/      237  78.06% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                      42/       49  85.71% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:      2021/    25298   7.99% 
Ghost tracks identified as muon with isMuon:                              103/      705  14.61% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.944 (  2769/  2933)
Isolated             :  0.979 (  1483/  1515)
Close                :  0.907 (  1286/  1418)
False rate           :  0.019 (    54/  2823)
Real false rate      :  0.019 (    54/  2823)
Clones               :  0.000 (     0/  2769)


rate_validator validation:
Hlt1BGIPseudoPVsBeamOne:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamTwo:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsIRBeamBeam:                         1/   500, (   60.00 +/-    59.94) kHz
Hlt1BGIPseudoPVsNoBeam:                             0/   500, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet100GeV:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet15GeV:                                   1/   500, (   60.00 +/-    59.94) kHz
Hlt1ConeJet30GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ConeJet50GeV:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1D2KK:                                          19/   500, ( 1140.00 +/-   256.52) kHz
Hlt1D2KPi:                                         22/   500, ( 1320.00 +/-   275.16) kHz
Hlt1D2KPiAlignment:                                 7/   500, (  420.00 +/-   157.63) kHz
Hlt1D2Kshh:                                         2/   500, (  120.00 +/-    84.68) kHz
Hlt1D2PiPi:                                        13/   500, (  780.00 +/-   213.50) kHz
Hlt1DetJpsiToMuMuNegTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronDisplaced:                            6/   500, (  360.00 +/-   146.08) kHz
Hlt1DiElectronHighMass:                             1/   500, (   60.00 +/-    59.94) kHz
Hlt1DiElectronHighMass_SS:                          1/   500, (   60.00 +/-    59.94) kHz
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice1_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice2_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice3_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_displaced:         0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronLowMass_massSlice4_prompt:            0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiElectronSoft:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDisplaced:                                6/   500, (  360.00 +/-   146.08) kHz
Hlt1DiMuonDrellYan:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:                                 5/   500, (  300.00 +/-   133.49) kHz
Hlt1DiMuonJpsiMassAlignment:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonNoIP_SS:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiMuonSoft:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1DiPhotonHighMass:                               3/   500, (  180.00 +/-   103.61) kHz
Hlt1Dst2D0Pi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0PiAlignment:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1GECPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1LambdaLLDetachedTrack:                          1/   500, (   60.00 +/-    59.94) kHz
Hlt1MaterialVertexSeedsDownstreamz:                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1ODINeeFarFromActivity:                          0/   500, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                               1/   500, (   60.00 +/-    59.94) kHz
Hlt1Passthrough:                                    0/   500, (    0.00 +/-     0.00) kHz
Hlt1PassthroughPVinSMOG2:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1Pi02GammaGamma:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGeneric:                              0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGenericPrompt:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BELowMultElectrons:                        0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BENoBias:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DiMuonHighMass:                            0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2DisplacedDiMuon:                           0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2L0Toppi:                                   0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2MinimumBias:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2PassThroughLowMult5:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleMuon:                                0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackHighPt:                         0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackVeryHighPt:                     0/   500, (    0.00 +/-     0.00) kHz
Hlt1SMOG2etacTopp:                                  0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtElectron:                           5/   500, (  300.00 +/-   133.49) kHz
Hlt1SingleHighPtMuon:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuonNoMuID:                         4/   500, (  240.00 +/-   119.52) kHz
Hlt1TAEPassthrough:                                 0/   500, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:                              23/   500, ( 1380.00 +/-   281.05) kHz
Hlt1TrackMVA:                                     115/   500, ( 6900.00 +/-   564.61) kHz
Hlt1TrackMuonMVA:                                   9/   500, (  540.00 +/-   178.37) kHz
Hlt1TwoKs:                                          1/   500, (   60.00 +/-    59.94) kHz
Hlt1TwoTrackKs:                                     3/   500, (  180.00 +/-   103.61) kHz
Hlt1TwoTrackMVA:                                  279/   500, (16740.00 +/-   666.29) kHz
Hlt1UpsilonAlignment:                               0/   500, (    0.00 +/-     0.00) kHz
Hlt1VeloMicroBias:                                  1/   500, (   60.00 +/-    59.94) kHz
Hlt1VeloMicroBiasVeloClosing:                       0/   500, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                                     0/   500, (    0.00 +/-     0.00) kHz
Inclusive:                                        307/   500, (18420.00 +/-   653.15) kHz


selreport_validator validation:
                                               Events  Candidates
Hlt1BGIPseudoPVsBeamOne:                            0           0
Hlt1BGIPseudoPVsBeamTwo:                            0           0
Hlt1BGIPseudoPVsDownBeamBeam:                       0           0
Hlt1BGIPseudoPVsIRBeamBeam:                         1           0
Hlt1BGIPseudoPVsNoBeam:                             0           0
Hlt1BGIPseudoPVsUpBeamBeam:                         0           0
Hlt1BeamGas:                                        0           0
Hlt1ConeJet100GeV:                                  0           0
Hlt1ConeJet15GeV:                                   1           1
Hlt1ConeJet30GeV:                                   0           0
Hlt1ConeJet50GeV:                                   0           0
Hlt1D2KK:                                          19          20
Hlt1D2KPi:                                         22          23
Hlt1D2KPiAlignment:                                 7           7
Hlt1D2Kshh:                                         2           3
Hlt1D2PiPi:                                        13          13
Hlt1DetJpsiToMuMuNegTagLine:                        0           0
Hlt1DetJpsiToMuMuPosTagLine:                        0           0
Hlt1DiElectronDisplaced:                            6           9
Hlt1DiElectronHighMass:                             1           2
Hlt1DiElectronHighMass_SS:                          1           5
Hlt1DiElectronLowMass_SS_massSlice1_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice1_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice2_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice2_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice3_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice3_prompt:         0           0
Hlt1DiElectronLowMass_SS_massSlice4_displaced:      0           0
Hlt1DiElectronLowMass_SS_massSlice4_prompt:         0           0
Hlt1DiElectronLowMass_massSlice1_displaced:         0           0
Hlt1DiElectronLowMass_massSlice1_prompt:            0           0
Hlt1DiElectronLowMass_massSlice2_displaced:         0           0
Hlt1DiElectronLowMass_massSlice2_prompt:            0           0
Hlt1DiElectronLowMass_massSlice3_displaced:         0           0
Hlt1DiElectronLowMass_massSlice3_prompt:            0           0
Hlt1DiElectronLowMass_massSlice4_displaced:         0           0
Hlt1DiElectronLowMass_massSlice4_prompt:            0           0
Hlt1DiElectronSoft:                                 0           0
Hlt1DiMuonDisplaced:                                6           6
Hlt1DiMuonDrellYan:                                 0           0
Hlt1DiMuonDrellYan_SS:                              0           0
Hlt1DiMuonDrellYan_VLowMass:                        0           0
Hlt1DiMuonDrellYan_VLowMass_SS:                     0           0
Hlt1DiMuonHighMass:                                 5           7
Hlt1DiMuonJpsiMassAlignment:                        0           0
Hlt1DiMuonNoIP:                                     0           0
Hlt1DiMuonNoIP_SS:                                  0           0
Hlt1DiMuonSoft:                                     0           0
Hlt1DiPhotonHighMass:                               3           6
Hlt1Dst2D0Pi:                                       0           0
Hlt1Dst2D0PiAlignment:                              0           0
Hlt1ErrorBank:                                      0           0
Hlt1GECPassthrough:                                 0           0
Hlt1KsToPiPi:                                       0           0
Hlt1KsToPiPiDoubleMuonMisID:                        0           0
Hlt1LambdaLLDetachedTrack:                          1           1
Hlt1MaterialVertexSeedsDownstreamz:                 0           0
Hlt1MaterialVertexSeeds_DWFS:                       0           0
Hlt1ODIN1kHzLumi:                                   0           0
Hlt1ODINCalib:                                      0           0
Hlt1ODINLumi:                                       0           0
Hlt1ODINeeFarFromActivity:                          0           0
Hlt1OneMuonTrackLine:                               1           0
Hlt1Passthrough:                                    0           0
Hlt1PassthroughPVinSMOG2:                           0           0
Hlt1Pi02GammaGamma:                                 0           0
Hlt1RICH1Alignment:                                 0           0
Hlt1RICH2Alignment:                                 0           0
Hlt1SMOG22BodyGeneric:                              0           0
Hlt1SMOG22BodyGenericPrompt:                        0           0
Hlt1SMOG2BELowMultElectrons:                        0           0
Hlt1SMOG2BENoBias:                                  0           0
Hlt1SMOG2D2Kpi:                                     0           0
Hlt1SMOG2DiMuonHighMass:                            0           0
Hlt1SMOG2DisplacedDiMuon:                           0           0
Hlt1SMOG2JPsiToMuMuTaP_NegTag:                      0           0
Hlt1SMOG2JPsiToMuMuTaP_PosTag:                      0           0
Hlt1SMOG2KsTopipi:                                  0           0
Hlt1SMOG2L0Toppi:                                   0           0
Hlt1SMOG2MinimumBias:                               0           0
Hlt1SMOG2PassThroughLowMult5:                       0           0
Hlt1SMOG2SingleMuon:                                0           0
Hlt1SMOG2SingleTrackHighPt:                         0           0
Hlt1SMOG2SingleTrackVeryHighPt:                     0           0
Hlt1SMOG2etacTopp:                                  0           0
Hlt1SingleHighPtElectron:                           5          10
Hlt1SingleHighPtMuon:                               0           0
Hlt1SingleHighPtMuonNoMuID:                         4           4
Hlt1TAEPassthrough:                                 0           0
Hlt1TrackElectronMVA:                              23          23
Hlt1TrackMVA:                                     115         177
Hlt1TrackMuonMVA:                                   9          10
Hlt1TwoKs:                                          1           1
Hlt1TwoTrackKs:                                     3           4
Hlt1TwoTrackMVA:                                  279         931
Hlt1UpsilonAlignment:                               0           0
Hlt1VeloMicroBias:                                  1           0
Hlt1VeloMicroBiasVeloClosing:                       0           0
Hlt1XiOmegaLLL:                                     0           0

Total decisions:      529
Total tracks:         1074
Total calos clusters: 10
Total SVs:            994
Total hits:           27981
Total stdinfo:        13137


veloUT_validator validation:
TrackChecker output                               :      2939/    35152   8.36% ghosts
01_velo                                           :     30320/    66132  45.85% ( 46.32%),       486 (  1.58%) clones, pur  98.52%, hit eff  93.78%
02_velo+UT                                        :     30246/    57399  52.69% ( 53.17%),       482 (  1.57%) clones, pur  98.54%, hit eff  93.77%
03_velo+UT_P>5GeV                                 :     21768/    29636  73.45% ( 74.13%),       384 (  1.73%) clones, pur  98.79%, hit eff  94.64%
04_velo+notLong                                   :      6163/    28206  21.85% ( 22.40%),        78 (  1.25%) clones, pur  98.02%, hit eff  92.51%
05_velo+UT+notLong                                :      6095/    19969  30.52% ( 31.27%),        74 (  1.20%) clones, pur  98.11%, hit eff  92.45%
06_velo+UT+notLong_P>5GeV                         :      3285/     5340  61.52% ( 62.96%),        52 (  1.56%) clones, pur  98.74%, hit eff  95.05%
07_long                                           :     24157/    37926  63.70% ( 64.06%),       408 (  1.66%) clones, pur  98.65%, hit eff  94.10%
08_long_P>5GeV                                    :     18489/    24714  74.81% ( 75.46%),       332 (  1.76%) clones, pur  98.80%, hit eff  94.57%
09_long_fromB                                     :      1941/     2309  84.06% ( 85.78%),        34 (  1.72%) clones, pur  98.60%, hit eff  94.34%
10_long_fromB_P>5GeV                              :      1725/     1909  90.36% ( 91.51%),        29 (  1.65%) clones, pur  98.68%, hit eff  94.54%
11_long_electrons                                 :       529/     2697  19.61% ( 19.77%),        12 (  2.22%) clones, pur  96.97%, hit eff  93.18%
12_long_fromB_electrons                           :        48/      110  43.64% ( 48.97%),         0 (  0.00%) clones, pur  97.64%, hit eff  92.92%
13_long_fromB_electrons_P>5GeV                    :        42/       74  56.76% ( 60.16%),         0 (  0.00%) clones, pur  97.87%, hit eff  94.29%


velo_validator validation:
TrackChecker output                               :      3258/   153826   2.12% ghosts
01_velo                                           :     65022/    66132  98.32% ( 98.45%),      1920 (  2.87%) clones, pur  99.66%, hit eff  95.37%
02_long                                           :     37670/    37926  99.33% ( 99.38%),       870 (  2.26%) clones, pur  99.78%, hit eff  96.58%
03_long_P>5GeV                                    :     24617/    24714  99.61% ( 99.63%),       521 (  2.07%) clones, pur  99.81%, hit eff  97.16%
04_long_strange                                   :      1754/     1807  97.07% ( 97.70%),        37 (  2.07%) clones, pur  99.42%, hit eff  96.30%
05_long_strange_P>5GeV                            :       852/      878  97.04% ( 97.01%),        14 (  1.62%) clones, pur  99.30%, hit eff  96.78%
06_long_fromB                                     :      2289/     2309  99.13% ( 99.33%),        45 (  1.93%) clones, pur  99.70%, hit eff  97.02%
07_long_fromB_P>5GeV                              :      1901/     1909  99.58% ( 99.63%),        36 (  1.86%) clones, pur  99.79%, hit eff  97.25%
08_long_electrons                                 :      2609/     2697  96.74% ( 97.23%),       112 (  4.12%) clones, pur  97.90%, hit eff  95.49%
09_long_fromB_electrons                           :       106/      110  96.36% ( 96.30%),         4 (  3.64%) clones, pur  98.84%, hit eff  96.25%
10_long_fromB_electrons_P>5GeV                    :        71/       74  95.95% ( 96.88%),         2 (  2.74%) clones, pur  98.36%, hit eff  96.71%
11_long_fromSignal                                :      1301/     1312  99.16% ( 99.19%),        22 (  1.66%) clones, pur  99.68%, hit eff  97.24%

