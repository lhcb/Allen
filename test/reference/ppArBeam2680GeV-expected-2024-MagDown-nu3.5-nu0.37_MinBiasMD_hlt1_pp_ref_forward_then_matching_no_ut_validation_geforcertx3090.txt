forward_validator validation:
TrackChecker output                               :        40/     3460   1.16% ghosts
for P>3GeV,Pt>0.5GeV                              :        40/     3460   1.16% ghosts
01_long                                           :      3288/    29988  10.96% (  9.73%),        67 (  2.00%) clones, pur  99.81%, hit eff  98.46%
02_long_P>5GeV                                    :      3258/    18751  17.38% ( 15.32%),        67 (  2.02%) clones, pur  99.82%, hit eff  98.50%
03_long_strange                                   :        69/     1562   4.42% (  3.50%),         0 (  0.00%) clones, pur  99.64%, hit eff  97.97%
04_long_strange_P>5GeV                            :        69/      733   9.41% (  8.26%),         0 (  0.00%) clones, pur  99.64%, hit eff  97.97%
05_long_fromB                                     :         1/       10  10.00% (  8.33%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
06_long_fromB_P>5GeV                              :         1/        7  14.29% ( 16.67%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
07_long_electrons                                 :        18/     2595   0.69% (  0.71%),         0 (  0.00%) clones, pur  98.57%, hit eff  99.03%
08_long_electrons_P>5GeV                          :        18/     1385   1.30% (  1.25%),         0 (  0.00%) clones, pur  98.57%, hit eff  99.03%
long_P>5GeV_AND_Pt>1GeV                           :      2283/     2668  85.57% ( 85.05%),        50 (  2.14%) clones, pur  99.80%, hit eff  99.28%
long_fromB_P>5GeV_AND_Pt>1GeV                     :         1/        1 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
11_noVelo_UT                                      :         0/     3618   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     1380   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :       363/      397  91.44% ( 91.38%),         5 (  1.36%) clones, pur  99.84%, hit eff  99.56%
15_long_strange_P>5GeV                            :        69/      733   9.41% (  8.26%),         0 (  0.00%) clones, pur  99.64%, hit eff  97.97%
16_long_strange_P>5GeV_PT>500MeV                  :        69/      253  27.27% ( 26.78%),         0 (  0.00%) clones, pur  99.64%, hit eff  97.97%
18_long_nSciFiHits_gt_0_AND_lt_5000               :      3114/    30964  10.06% (  9.01%),        60 (  1.89%) clones, pur  99.81%, hit eff  98.50%
19_long_nSciFiHits_gt_5000_AND_lt_7000            :       192/     1619  11.86% ( 11.66%),         7 (  3.52%) clones, pur  99.64%, hit eff  97.98%


long_validator validation:
TrackChecker output                               :       739/    23104   3.20% ghosts
for P>3GeV,Pt>0.5GeV                              :       124/     9687   1.28% ghosts
01_long                                           :     20312/    29988  67.73% ( 68.43%),       457 (  2.20%) clones, pur  99.87%, hit eff  98.64%
02_long_P>5GeV                                    :     16276/    18751  86.80% ( 87.12%),       390 (  2.34%) clones, pur  99.88%, hit eff  98.95%
03_long_strange                                   :       879/     1562  56.27% ( 54.92%),        20 (  2.22%) clones, pur  99.83%, hit eff  98.76%
04_long_strange_P>5GeV                            :       608/      733  82.95% ( 83.77%),        13 (  2.09%) clones, pur  99.79%, hit eff  98.96%
05_long_fromB                                     :         8/       10  80.00% ( 86.67%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.86%
06_long_fromB_P>5GeV                              :         6/        7  85.71% ( 87.50%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
07_long_electrons                                 :       895/     2595  34.49% ( 34.45%),        36 (  3.87%) clones, pur  99.00%, hit eff  98.54%
08_long_electrons_P>5GeV                          :       704/     1385  50.83% ( 52.44%),        24 (  3.30%) clones, pur  98.95%, hit eff  98.59%
long_P>5GeV_AND_Pt>1GeV                           :      2482/     2668  93.03% ( 92.86%),        53 (  2.09%) clones, pur  99.81%, hit eff  99.21%
long_fromB_P>5GeV_AND_Pt>1GeV                     :         1/        1 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
11_noVelo_UT                                      :         0/     3618   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
12_noVelo_UT_P>5GeV                               :         0/     1380   0.00% (  0.00%),         0 (  0.00%) clones, pur   -nan%, hit eff   -nan%
13_long_PT>2GeV                                   :       376/      397  94.71% ( 94.85%),         5 (  1.31%) clones, pur  99.84%, hit eff  99.58%
15_long_strange_P>5GeV                            :       608/      733  82.95% ( 83.77%),        13 (  2.09%) clones, pur  99.79%, hit eff  98.96%
16_long_strange_P>5GeV_PT>500MeV                  :       222/      253  87.75% ( 86.55%),         4 (  1.77%) clones, pur  99.79%, hit eff  98.64%
18_long_nSciFiHits_gt_0_AND_lt_5000               :     20162/    30964  65.11% ( 65.66%),       468 (  2.27%) clones, pur  99.84%, hit eff  98.69%
19_long_nSciFiHits_gt_5000_AND_lt_7000            :      1045/     1619  64.55% ( 64.75%),        25 (  2.34%) clones, pur  99.71%, hit eff  97.70%


muon_validator validation:
Muon fraction in all MCPs:                                               5629/   469470   0.01% 
Muon fraction in MCPs to which a track(s) was matched:                    186/    25377   0.01% 
Correctly identified muons with isMuon:                                   170/      186  91.40% 
Correctly identified muons from strange decays with isMuon:                 0/        0   -nan% 
Correctly identified muons from B decays with isMuon:                       0/        0   -nan% 
Tracks identified as muon with isMuon, but matched to non-muon MCP:       849/    25191   3.37% 
Ghost tracks identified as muon with isMuon:                               46/      739   6.22% 


pv_validator validation:
REC and MC vertices matched by dz distance
MC PV is reconstructible if at least 4 tracks are reconstructed
MC PV is isolated if dz to closest reconstructible MC PV > 10.00 mm
REC and MC vertices matched by dz distance

All                  :  0.911 (  3167/  3476)
Isolated             :  0.934 (  2278/  2439)
Close                :  0.857 (   889/  1037)
False rate           :  0.015 (    48/  3215)
Real false rate      :  0.015 (    48/  3215)
Clones               :  0.000 (     0/  3167)


rate_validator validation:
Hlt1BGIPseudoPVsBeamOne:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsBeamTwo:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsDownBeamBeam:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsNoBeam:                  0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BGIPseudoPVsUpBeamBeam:              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1BeamGas:                             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ConeJet15GeV:                        1/  1000, (   30.00 +/-    29.98) kHz
Hlt1ConeJet30GeV:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ConeJet50GeV:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1D2KPi:                               5/  1000, (  150.00 +/-    66.91) kHz
Hlt1D2KPiAlignment:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuNegTagLine:             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DetJpsiToMuMuPosTagLine:             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDisplaced:                     1/  1000, (   30.00 +/-    29.98) kHz
Hlt1DiMuonDrellYan:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_SS:                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass:             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonDrellYan_VLowMass_SS:          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonHighMass:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1DiMuonJpsiMassAlignment:             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1Dst2D0PiAlignment:                   0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ErrorBank:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1GECPassthrough:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1HighPtPhoton:                        3/  1000, (   90.00 +/-    51.88) kHz
Hlt1KsToPiPi:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1KsToPiPiDoubleMuonMisID:             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1LambdaLLDetachedTrack:               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeedsDownstreamz:      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1MaterialVertexSeeds_DWFS:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODIN1kHzLumi:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINCalib:                           0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINLumi:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1ODINeeFarFromActivity:               0/  1000, (    0.00 +/-     0.00) kHz
Hlt1OneMuonTrackLine:                    1/  1000, (   30.00 +/-    29.98) kHz
Hlt1Passthrough:                         1/  1000, (   30.00 +/-    29.98) kHz
Hlt1PassthroughPVinSMOG2:                0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH1Alignment:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1RICH2Alignment:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG22BodyGeneric:                  15/  1000, (  450.00 +/-   115.31) kHz
Hlt1SMOG22BodyGenericPrompt:             1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2BELowMultElectrons:             0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2BENoBias:                       0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2D2Kpi:                          7/  1000, (  210.00 +/-    79.09) kHz
Hlt1SMOG2DiMuonHighMass:                 0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2KsTopipi:                       3/  1000, (   90.00 +/-    51.88) kHz
Hlt1SMOG2L0Toppi:                        4/  1000, (  120.00 +/-    59.88) kHz
Hlt1SMOG2MinimumBias:                    1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2PassThroughLowMult5:            1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SMOG2SingleMuon:                     2/  1000, (   60.00 +/-    42.38) kHz
Hlt1SMOG2SingleTrackHighPt:              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2SingleTrackVeryHighPt:          0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SMOG2etacTopp:                       1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SingleHighPtElectron:                1/  1000, (   30.00 +/-    29.98) kHz
Hlt1SingleHighPtMuon:                    0/  1000, (    0.00 +/-     0.00) kHz
Hlt1SingleHighPtMuonNoMuID:              0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TAEPassthrough:                      0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackElectronMVA:                    2/  1000, (   60.00 +/-    42.38) kHz
Hlt1TrackMVA:                            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TrackMuonMVA:                        0/  1000, (    0.00 +/-     0.00) kHz
Hlt1TwoTrackMVA:                         6/  1000, (  180.00 +/-    73.26) kHz
Hlt1VeloMicroBias:                       1/  1000, (   30.00 +/-    29.98) kHz
Hlt1VeloMicroBiasVeloClosing:            0/  1000, (    0.00 +/-     0.00) kHz
Hlt1XiOmegaLLL:                          0/  1000, (    0.00 +/-     0.00) kHz
Inclusive:                              47/  1000, ( 1410.00 +/-   200.78) kHz


seed_validator validation:
TrackChecker output                               :       296/    35560   0.83% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :      6466/    10586  61.08% ( 64.99%),        37 (  0.57%) clones, pur  99.68%, hit eff  98.24%
01_long                                           :     20028/    29988  66.79% ( 68.81%),       123 (  0.61%) clones, pur  99.71%, hit eff  98.36%
---1. phi quadrant                                :      4946/     7355  67.25% ( 69.49%),        41 (  0.82%) clones, pur  99.70%, hit eff  98.23%
---2. phi quadrant                                :      5071/     7652  66.27% ( 68.23%),        32 (  0.63%) clones, pur  99.72%, hit eff  98.41%
---3. phi quadrant                                :      5000/     7533  66.37% ( 67.20%),        31 (  0.62%) clones, pur  99.70%, hit eff  98.37%
---4. phi quadrant                                :      5011/     7448  67.28% ( 69.19%),        19 (  0.38%) clones, pur  99.73%, hit eff  98.44%
---eta < 2.5, small x, large y                    :       105/      985  10.66% (  9.58%),         0 (  0.00%) clones, pur  99.09%, hit eff  91.17%
---eta < 2.5, large x, small y                    :       414/     1610  25.71% ( 25.65%),         5 (  1.19%) clones, pur  99.31%, hit eff  95.73%
---eta > 2.5, small x, large y                    :      7272/    10039  72.44% ( 74.52%),        50 (  0.68%) clones, pur  99.72%, hit eff  98.21%
---eta > 2.5, large x, small y                    :     12237/    17354  70.51% ( 72.24%),        68 (  0.55%) clones, pur  99.73%, hit eff  98.60%
02_long_P>5GeV                                    :     14830/    18751  79.09% ( 81.57%),        94 (  0.63%) clones, pur  99.72%, hit eff  98.78%
02_long_P>5GeV, eta > 4                           :      7549/     8659  87.18% ( 88.31%),        54 (  0.71%) clones, pur  99.67%, hit eff  98.74%
---eta < 2.5, small x, large y                    :        44/      199  22.11% ( 21.37%),         0 (  0.00%) clones, pur  99.34%, hit eff  94.35%
---eta < 2.5, large x, small y                    :       121/      350  34.57% ( 36.82%),         0 (  0.00%) clones, pur  99.86%, hit eff  98.39%
---eta > 2.5, small x, large y                    :      5577/     6767  82.41% ( 84.74%),        41 (  0.73%) clones, pur  99.70%, hit eff  98.75%
---eta > 2.5, large x, small y                    :      9088/    11435  79.48% ( 81.90%),        53 (  0.58%) clones, pur  99.73%, hit eff  98.82%
03_long_P>3GeV                                    :     20019/    25297  79.14% ( 81.17%),       123 (  0.61%) clones, pur  99.71%, hit eff  98.36%
04_long_P>0.5GeV                                  :     20028/    29988  66.79% ( 68.81%),       123 (  0.61%) clones, pur  99.71%, hit eff  98.36%
05_long_from_B                                    :         7/       10  70.00% ( 78.33%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.70%
06_long_from_B_P>5GeV                             :         5/        7  71.43% ( 70.83%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
07_long_from_B_P>3GeV                             :         7/        9  77.78% ( 83.33%),         0 (  0.00%) clones, pur 100.00%, hit eff  98.70%
08_UT+SciFi                                       :      2371/     4561  51.98% ( 50.58%),        11 (  0.46%) clones, pur  99.69%, hit eff  97.57%
09_UT+SciFi_P>5GeV                                :      1487/     1648  90.23% ( 90.57%),         7 (  0.47%) clones, pur  99.81%, hit eff  98.16%
10_UT+SciFi_P>3GeV                                :      2349/     2918  80.50% ( 80.30%),        11 (  0.47%) clones, pur  99.69%, hit eff  97.58%
11_UT+SciFi_fromStrange                           :       897/     1278  70.19% ( 70.15%),         2 (  0.22%) clones, pur  99.87%, hit eff  98.53%
12_UT+SciFi_fromStrange_P>5GeV                    :       658/      696  94.54% ( 95.31%),         2 (  0.30%) clones, pur  99.90%, hit eff  98.80%
13_UT+SciFi_fromStrange_P>3GeV                    :       894/      999  89.49% ( 90.03%),         2 (  0.22%) clones, pur  99.86%, hit eff  98.53%
14_long_electrons                                 :      1352/     2595  52.10% ( 51.09%),         4 (  0.29%) clones, pur  99.81%, hit eff  98.47%
15_long_electrons_P>5GeV                          :      1035/     1385  74.73% ( 74.70%),         4 (  0.38%) clones, pur  99.79%, hit eff  98.56%
16_long_electrons_P>3GeV                          :      1352/     2126  63.59% ( 63.40%),         4 (  0.29%) clones, pur  99.81%, hit eff  98.47%
19_long_PT>2GeV                                   :        21/      397   5.29% (  5.44%),         0 (  0.00%) clones, pur 100.00%, hit eff  99.24%
21_long_strange_P>5GeV                            :       633/      733  86.36% ( 87.53%),         4 (  0.63%) clones, pur  99.73%, hit eff  98.90%
22_long_strange_P>5GeV_PT>500MeV                  :       170/      253  67.19% ( 67.64%),         0 (  0.00%) clones, pur  99.95%, hit eff  98.87%
24_noVelo+UT+T_fromKs0                            :       576/      826  69.73% ( 66.95%),         2 (  0.35%) clones, pur  99.89%, hit eff  98.61%
25_noVelo+UT+T_fromLambda                         :       319/      451  70.73% ( 73.57%),         1 (  0.31%) clones, pur  99.92%, hit eff  98.42%
27_noVelo+UT+T_fromKs0_P>5GeV                     :       419/      439  95.44% ( 95.72%),         2 (  0.48%) clones, pur  99.89%, hit eff  98.99%
28_noVelo+UT+T_fromLambda_P>5GeV                  :       233/      252  92.46% ( 93.74%),         1 (  0.43%) clones, pur  99.96%, hit eff  98.63%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       201/      212  94.81% ( 95.22%),         0 (  0.00%) clones, pur  99.96%, hit eff  99.15%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       149/      163  91.41% ( 92.64%),         1 (  0.67%) clones, pur 100.00%, hit eff  98.86%
35_long_nSciFiHits_gt_0_AND_lt_5000               :     22278/    33890  65.74% ( 67.27%),       130 (  0.58%) clones, pur  99.73%, hit eff  98.44%
36_long_nSciFiHits_gt_5000_AND_lt_7000            :      1136/     1785  63.64% ( 63.90%),        15 (  1.30%) clones, pur  99.30%, hit eff  97.18%


seed_xz_validator validation:
TrackChecker output                               :      4336/    47995   9.03% ghosts
for P>3GeV,Pt>0.5GeV                              :         0/        0   -nan% ghosts
00_P>3Gev_Pt>0.5                                  :     10049/    10586  94.93% ( 95.26%),        64 (  0.63%) clones, pur  99.64%, hit eff  49.49%
01_long                                           :     24040/    29988  80.17% ( 80.91%),       155 (  0.64%) clones, pur  99.68%, hit eff  49.46%
---1. phi quadrant                                :      5930/     7355  80.63% ( 81.46%),        33 (  0.55%) clones, pur  99.75%, hit eff  49.47%
---2. phi quadrant                                :      6131/     7652  80.12% ( 80.37%),        37 (  0.60%) clones, pur  99.69%, hit eff  49.48%
---3. phi quadrant                                :      6013/     7533  79.82% ( 79.39%),        46 (  0.76%) clones, pur  99.63%, hit eff  49.45%
---4. phi quadrant                                :      5966/     7448  80.10% ( 80.43%),        39 (  0.65%) clones, pur  99.65%, hit eff  49.45%
---eta < 2.5, small x, large y                    :       294/      985  29.85% ( 27.72%),         1 (  0.34%) clones, pur  98.14%, hit eff  48.23%
---eta < 2.5, large x, small y                    :       699/     1610  43.42% ( 41.91%),         3 (  0.43%) clones, pur  99.62%, hit eff  49.38%
---eta > 2.5, small x, large y                    :      8502/    10039  84.69% ( 85.38%),        55 (  0.64%) clones, pur  99.70%, hit eff  49.43%
---eta > 2.5, large x, small y                    :     14545/    17354  83.81% ( 84.12%),        96 (  0.66%) clones, pur  99.70%, hit eff  49.50%
02_long_P>5GeV                                    :     18430/    18751  98.29% ( 98.48%),       124 (  0.67%) clones, pur  99.69%, hit eff  49.52%
02_long_P>5GeV, eta > 4                           :      8534/     8659  98.56% ( 98.61%),        58 (  0.68%) clones, pur  99.66%, hit eff  49.55%
---eta < 2.5, small x, large y                    :       176/      199  88.44% ( 87.89%),         1 (  0.56%) clones, pur  98.70%, hit eff  48.81%
---eta < 2.5, large x, small y                    :       338/      350  96.57% ( 96.31%),         1 (  0.29%) clones, pur  99.83%, hit eff  49.62%
---eta > 2.5, small x, large y                    :      6668/     6767  98.54% ( 98.71%),        42 (  0.63%) clones, pur  99.69%, hit eff  49.45%
---eta > 2.5, large x, small y                    :     11248/    11435  98.36% ( 98.48%),        80 (  0.71%) clones, pur  99.70%, hit eff  49.57%
03_long_P>3GeV                                    :     24029/    25297  94.99% ( 95.44%),       155 (  0.64%) clones, pur  99.68%, hit eff  49.46%
04_long_P>0.5GeV                                  :     24040/    29988  80.17% ( 80.91%),       155 (  0.64%) clones, pur  99.68%, hit eff  49.46%
05_long_from_B                                    :         9/       10  90.00% ( 93.33%),         1 ( 10.00%) clones, pur  98.00%, hit eff  47.50%
06_long_from_B_P>5GeV                             :         7/        7 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff  49.46%
07_long_from_B_P>3GeV                             :         9/        9 100.00% (100.00%),         1 ( 10.00%) clones, pur  98.00%, hit eff  47.50%
08_UT+SciFi                                       :      2682/     4561  58.80% ( 57.25%),        19 (  0.70%) clones, pur  99.65%, hit eff  49.36%
09_UT+SciFi_P>5GeV                                :      1612/     1648  97.82% ( 98.22%),        11 (  0.68%) clones, pur  99.77%, hit eff  49.48%
10_UT+SciFi_P>3GeV                                :      2653/     2918  90.92% ( 90.92%),        19 (  0.71%) clones, pur  99.66%, hit eff  49.37%
11_UT+SciFi_fromStrange                           :       941/     1278  73.63% ( 74.00%),         3 (  0.32%) clones, pur  99.78%, hit eff  49.48%
12_UT+SciFi_fromStrange_P>5GeV                    :       681/      696  97.84% ( 98.58%),         2 (  0.29%) clones, pur  99.84%, hit eff  49.56%
13_UT+SciFi_fromStrange_P>3GeV                    :       938/      999  93.89% ( 94.82%),         3 (  0.32%) clones, pur  99.78%, hit eff  49.46%
14_long_electrons                                 :      1411/     2595  54.37% ( 53.20%),         3 (  0.21%) clones, pur  99.59%, hit eff  49.21%
15_long_electrons_P>5GeV                          :      1087/     1385  78.48% ( 78.19%),         3 (  0.28%) clones, pur  99.55%, hit eff  49.24%
16_long_electrons_P>3GeV                          :      1411/     2126  66.37% ( 66.08%),         3 (  0.21%) clones, pur  99.59%, hit eff  49.21%
19_long_PT>2GeV                                   :       391/      397  98.49% ( 98.35%),         1 (  0.26%) clones, pur  99.63%, hit eff  49.85%
21_long_strange_P>5GeV                            :       719/      733  98.09% ( 97.74%),         3 (  0.42%) clones, pur  99.74%, hit eff  49.55%
22_long_strange_P>5GeV_PT>500MeV                  :       247/      253  97.63% ( 97.46%),         1 (  0.40%) clones, pur  99.60%, hit eff  49.24%
24_noVelo+UT+T_fromKs0                            :       601/      826  72.76% ( 70.34%),         2 (  0.33%) clones, pur  99.81%, hit eff  49.59%
25_noVelo+UT+T_fromLambda                         :       340/      451  75.39% ( 78.98%),         2 (  0.58%) clones, pur  99.67%, hit eff  49.36%
27_noVelo+UT+T_fromKs0_P>5GeV                     :       430/      439  97.95% ( 97.99%),         1 (  0.23%) clones, pur  99.83%, hit eff  49.71%
28_noVelo+UT+T_fromLambda_P>5GeV                  :       246/      252  97.62% ( 98.33%),         2 (  0.81%) clones, pur  99.77%, hit eff  49.36%
30_noVelo+UT+T_fromKs0_P>5GeV_PT>500MeV           :       210/      212  99.06% ( 99.09%),         0 (  0.00%) clones, pur  99.83%, hit eff  49.77%
31_noVelo+UT+T_fromLambda_P>5GeV_PT>500MeV        :       160/      163  98.16% ( 98.28%),         2 (  1.23%) clones, pur  99.75%, hit eff  49.41%
35_long_nSciFiHits_gt_0_AND_lt_5000               :     26211/    33890  77.34% ( 77.88%),       160 (  0.61%) clones, pur  99.68%, hit eff  49.49%
36_long_nSciFiHits_gt_5000_AND_lt_7000            :      1385/     1785  77.59% ( 77.65%),        10 (  0.72%) clones, pur  99.39%, hit eff  48.83%


selreport_validator validation:
                                    Events  Candidates
Hlt1BGIPseudoPVsBeamOne:                 0           0
Hlt1BGIPseudoPVsBeamTwo:                 0           0
Hlt1BGIPseudoPVsDownBeamBeam:            0           0
Hlt1BGIPseudoPVsNoBeam:                  0           0
Hlt1BGIPseudoPVsUpBeamBeam:              0           0
Hlt1BeamGas:                             0           0
Hlt1ConeJet15GeV:                        1           1
Hlt1ConeJet30GeV:                        0           0
Hlt1ConeJet50GeV:                        0           0
Hlt1D2KPi:                               5           6
Hlt1D2KPiAlignment:                      0           0
Hlt1DetJpsiToMuMuNegTagLine:             0           0
Hlt1DetJpsiToMuMuPosTagLine:             0           0
Hlt1DiMuonDisplaced:                     1           1
Hlt1DiMuonDrellYan:                      0           0
Hlt1DiMuonDrellYan_SS:                   0           0
Hlt1DiMuonDrellYan_VLowMass:             0           0
Hlt1DiMuonDrellYan_VLowMass_SS:          0           0
Hlt1DiMuonHighMass:                      0           0
Hlt1DiMuonJpsiMassAlignment:             0           0
Hlt1Dst2D0PiAlignment:                   0           0
Hlt1ErrorBank:                           0           0
Hlt1GECPassthrough:                      0           0
Hlt1HighPtPhoton:                        3           3
Hlt1KsToPiPi:                            0           0
Hlt1KsToPiPiDoubleMuonMisID:             0           0
Hlt1LambdaLLDetachedTrack:               0           0
Hlt1MaterialVertexSeedsDownstreamz:      0           0
Hlt1MaterialVertexSeeds_DWFS:            0           0
Hlt1ODIN1kHzLumi:                        0           0
Hlt1ODINCalib:                           0           0
Hlt1ODINLumi:                            0           0
Hlt1ODINeeFarFromActivity:               0           0
Hlt1OneMuonTrackLine:                    1           0
Hlt1Passthrough:                         1           0
Hlt1PassthroughPVinSMOG2:                0           0
Hlt1RICH1Alignment:                      0           0
Hlt1RICH2Alignment:                      0           0
Hlt1SMOG22BodyGeneric:                  15          46
Hlt1SMOG22BodyGenericPrompt:             1           5
Hlt1SMOG2BELowMultElectrons:             0           0
Hlt1SMOG2BENoBias:                       0           0
Hlt1SMOG2D2Kpi:                          7          12
Hlt1SMOG2DiMuonHighMass:                 0           0
Hlt1SMOG2KsTopipi:                       3           6
Hlt1SMOG2L0Toppi:                        4           4
Hlt1SMOG2MinimumBias:                    1           0
Hlt1SMOG2PassThroughLowMult5:            1           0
Hlt1SMOG2SingleMuon:                     2           2
Hlt1SMOG2SingleTrackHighPt:              0           0
Hlt1SMOG2SingleTrackVeryHighPt:          0           0
Hlt1SMOG2etacTopp:                       1           1
Hlt1SingleHighPtElectron:                1           1
Hlt1SingleHighPtMuon:                    0           0
Hlt1SingleHighPtMuonNoMuID:              0           0
Hlt1TAEPassthrough:                      0           0
Hlt1TrackElectronMVA:                    2           2
Hlt1TrackMVA:                            0           0
Hlt1TrackMuonMVA:                        0           0
Hlt1TwoTrackMVA:                         6          10
Hlt1VeloMicroBias:                       1           0
Hlt1VeloMicroBiasVeloClosing:            0           0
Hlt1XiOmegaLLL:                          0           0

Total decisions:      57
Total tracks:         108
Total calos clusters: 4
Total SVs:            81
Total hits:           2700
Total stdinfo:        1261


velo_validator validation:
TrackChecker output                               :       826/   114150   0.72% ghosts
01_velo                                           :     52338/    53266  98.26% ( 98.35%),      2279 (  4.17%) clones, pur  99.69%, hit eff  93.97%
02_long                                           :     29752/    29988  99.21% ( 99.25%),      1234 (  3.98%) clones, pur  99.81%, hit eff  94.81%
03_long_P>5GeV                                    :     18653/    18751  99.48% ( 99.54%),       803 (  4.13%) clones, pur  99.82%, hit eff  95.01%
04_long_strange                                   :      1530/     1562  97.95% ( 98.15%),        56 (  3.53%) clones, pur  99.47%, hit eff  95.19%
05_long_strange_P>5GeV                            :       718/      733  97.95% ( 97.98%),        23 (  3.10%) clones, pur  99.25%, hit eff  96.14%
06_long_fromB                                     :        10/       10 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
07_long_fromB_P>5GeV                              :         7/        7 100.00% (100.00%),         0 (  0.00%) clones, pur 100.00%, hit eff 100.00%
08_long_electrons                                 :      2526/     2595  97.34% ( 97.42%),       192 (  7.06%) clones, pur  97.37%, hit eff  92.67%

