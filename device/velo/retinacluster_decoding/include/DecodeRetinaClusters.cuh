/*****************************************************************************\
* (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <cstdint>
#include <cassert>
#include "ClusteringDefinitions.cuh"
#include "VeloEventModel.cuh"
#include "AlgorithmTypes.cuh"
#include "VeloTools.cuh"

namespace decode_retinaclusters {
  struct Parameters {
    HOST_INPUT(host_total_number_of_velo_clusters_t, unsigned) host_total_number_of_velo_clusters;
    HOST_INPUT(host_raw_bank_version_t, int) host_raw_bank_version;
    HOST_INPUT(host_number_of_events_t, unsigned) host_number_of_events;
    DEVICE_INPUT(dev_velo_retina_raw_input_t, char) dev_velo_retina_raw_input;
    DEVICE_INPUT(dev_velo_retina_raw_input_offsets_t, unsigned) dev_velo_retina_raw_input_offsets;
    DEVICE_INPUT(dev_velo_retina_raw_input_sizes_t, unsigned) dev_velo_retina_raw_input_sizes;
    DEVICE_INPUT(dev_velo_retina_raw_input_types_t, unsigned) dev_velo_retina_raw_input_types;
    DEVICE_INPUT(dev_offsets_each_sensor_pair_size_t, unsigned) dev_offsets_each_sensor_pair_size;
    DEVICE_INPUT(dev_retina_bank_index_t, unsigned) dev_retina_bank_index;
    MASK_INPUT(dev_event_list_t) dev_event_list;
    DEVICE_INPUT(dev_number_of_events_t, unsigned) dev_number_of_events;
    DEVICE_OUTPUT(dev_module_cluster_num_t, unsigned) dev_module_pair_cluster_num;
    DEVICE_OUTPUT(dev_offsets_module_pair_cluster_t, unsigned) dev_offsets_module_pair_cluster;
    DEVICE_OUTPUT(dev_velo_cluster_container_t, char) dev_velo_cluster_container;
    DEVICE_OUTPUT(dev_hit_permutations_t, unsigned) dev_hit_permutations;
    DEVICE_OUTPUT(dev_hit_sorting_key_t, int64_t) dev_hit_sorting_key;
    DEVICE_OUTPUT_WITH_DEPENDENCIES(
      dev_velo_clusters_t,
      DEPENDENCIES(
        dev_velo_cluster_container_t,
        dev_module_cluster_num_t,
        dev_number_of_events_t,
        dev_offsets_module_pair_cluster_t),
      Velo::Clusters)
    dev_velo_clusters;
  };

  // Define postconditions
  struct cluster_container_checks : public Allen::contract::Postcondition {
    void operator()(
      const ArgumentReferences<Parameters>&,
      const RuntimeOptions&,
      const Constants&,
      const Allen::Context&) const;
  };

  struct decode_retinaclusters_t : public DeviceAlgorithm, Parameters {

    using contracts = std::tuple<cluster_container_checks>;

    void set_arguments_size(ArgumentReferences<Parameters> arguments, const RuntimeOptions&, const Constants&) const;

    void operator()(
      const ArgumentReferences<Parameters>& arguments,
      const RuntimeOptions& runtime_options,
      const Constants&,
      const Allen::Context& context) const;

  private:
    Allen::Property<unsigned> m_block_dim_x_calculate_key {this,
                                                           "block_dim_x_calculate_key",
                                                           256,
                                                           "block dim x of calculate_key"};
    Allen::Property<dim3> m_block_dim_calculate_permutations {this,
                                                              "block_dim_calculate_permutations",
                                                              {128, 2, 1},
                                                              "block dims of calculate permutations"};
    Allen::Property<unsigned> m_block_dim_x_decode_retina {this,
                                                           "block_dim_x_decode_retina",
                                                           256,
                                                           "block dim x of decode retina sorted"};
  };
} // namespace decode_retinaclusters
