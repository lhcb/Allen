###############################################################################
# (c) Copyright 2018-2020 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the Apache License          #
# version 2 (Apache-2.0), copied verbatim in the file "LICENSE".              #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
add_subdirectory(data_provider)
add_subdirectory(global_event_cut)
add_subdirectory(error_banks_cut)
add_subdirectory(velo/clustering)
add_subdirectory(init_event_list)
add_subdirectory(validators)
add_subdirectory(combiners)
add_subdirectory(event_model)
add_subdirectory(routing_bits)
add_subdirectory(dummy_maker)
add_subdirectory(tae)
